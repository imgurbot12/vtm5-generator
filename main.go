package main

import (
	"github.com/kataras/iris"

	"./db"
)

/* Functions */

// errorResponse : basic error response on error
func errorResponse(err error, ctx iris.Context) {
	ctx.Text("500 - Internal Error!")
	ctx.StatusCode(500)
}

// apiHandler : wrapper for existing functions that works with database functions
func apiHandler(f func(iris.Context) (interface{}, error)) func(iris.Context) {
	return func(ctx iris.Context) {
		json, err := f(ctx)
		if err != nil {
			ctx.Text("500 - Internal Error!")
			ctx.StatusCode(500)
		} else {
			ctx.JSON(json)
		}
	}
}

/* REST API */

func main() {
	app := iris.Default()
	app.Get("/api/clans/", apiHandler(db.GetClans))
	app.Get("/api/clans/disciplines/{clan:string}/", apiHandler(db.GetClanDisciplines))
	app.Get("/api/disciplines/powers/{discipline:string}/{level:int}/", apiHandler(db.GetClanDisciplines))
	//app.Get("/api/disciplines/description/{discipline:string}")
	app.StaticWeb("/js/", "js/")
	app.StaticWeb("/css/", "css/")
	app.StaticWeb("/fonts/", "fonts/")
	app.StaticWeb("/images/", "images/")
	app.StaticWeb("/", "html/index.html")
	app.Run(iris.Addr(":8080"), iris.WithoutServerError(iris.ErrServerClosed))
}
