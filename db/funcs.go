package db

import (
	"database/sql"
	"github.com/kataras/iris"
	_ "github.com/mattn/go-sqlite3" // sqlite3 driver
	"log"
)

/* Variables */
var db *sql.DB

/* Functions */

//getSingleObject : execute sql statement with given arguments, retrieve single returned object per row
// and return list of objects
func getSingleObject(query string, args ...interface{}) (interface{}, error) {
	// attempt to retrieve rows using given query and arguments
	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	// complete scan of single object from rows
	var out string
	var objs []string
	for rows.Next() {
		if err = rows.Scan(&out); err != nil {
			return nil, err
		}
		objs = append(objs, out)
	}
	return objs, nil
}

//getDoubleObj : execute sql statement with given args, retreive two returned objects per row
// and return a list of objects
func getDoubleObject(query string, args ...interface{}) (interface{}, error) {
	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	// complete scan of single object from rows
	var objs [][]string
	var out1, out2 string
	for rows.Next() {
		if err = rows.Scan(&out1, &out2); err != nil {
			return nil, err
		}
		objs = append(objs, []string{out1, out2})
	}
	return objs, nil
}

//GetClans : return list of clans
func GetClans(_ iris.Context) (interface{}, error) {
	return getSingleObject("SELECT ClanName FROM Clans WHERE LogicalDelete=0")
}

//GetClanDisciplines : get list of disciplines associated with a particular clan
func GetClanDisciplines(ctx iris.Context) (interface{}, error) {
	clanName := ctx.Params().GetString("clan")
	return getSingleObject("SELECT Discipline FROM rDisciplineToClan WHERE ClanName=? AND LogicalDelete=0", clanName)
}

//GetDisciplinePowers : get list of powers associated with a particular clan and powerlevel
func GetDisciplinePowers(ctx iris.Context) (interface{}, error) {
	discipline := ctx.Params().GetString("discpline")
	powerLevel, err := ctx.Params().GetInt("level")
	if err != nil {
		return nil, err
	}
	return getDoubleObject(
		"SELECT PowerName,PowerLevel FROM rPowerToDiscipline WHERE Discipline=? AND LogicalDelete=0 AND PowerLevel<=?",
		discipline, powerLevel)
}

func GetDisciplineDescription() {

}

/* Init */
func init() {
	// attempt to open database connection
	var err error
	db, err = sql.Open("sqlite3", "db/sql/vampire.db")
	if err != nil {
		log.Fatalf("SQL-Open-Err: %s\n", err.Error())
	}
	// attempt to ping database
	if err = db.Ping(); err != nil {
		log.Fatalf("SQL-Ping-Err: %s\n", err.Error())
	}
}
