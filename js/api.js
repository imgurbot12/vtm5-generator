
/* Variables */
const _baseapi = 'http://'+window.location.host+'/api/';

//apiRequest : complete api request and run callback on completion
function _apiRequest(uri, callback) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', uri, false);
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            callback(JSON.parse(xmlhttp.responseText));
        }
    };
    xmlhttp.send(null);
}

//getClans : update clan select menu using clan-list collected via local rest-api
function apiGetClans() {
    _apiRequest(_baseapi+'clans/', function(clanList) {
        let select = document.getElementById('clan');
        // iterate list of clans retrieved and make options under clan select
        for (i=0; i < clanList.length; i++) {
            let clan = clanList[i];
            let option = document.createElement('option');
            option.value = clan;
            option.innerHTML = clan;
            select.appendChild(option);
        }
    });
}

//apiGetDisciplines : get given disciplines for specified clan name
function apiGetDisciplines(clan, callback) {

    _apiRequest(_baseapi+'clans/disciplines/'+clan+'/', callback);
}

//apiGetPowers : get powers for discipline up to given level
function apiGetPowers(discipline, level, callback) {
    _apiRequest(_baseapi+'disciplines/'+discipline+'/'+level.toString()+'/', callback);
}

function init() {
    apiGetClans(); // get base list of clans via rest-api
}

// run init
init();