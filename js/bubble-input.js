
/* Functions */

//makeBubble : return bubble object created using javascript
function makeBubble(attribute_name, input_class) {
    let container = document.createElement('div');
    container.classList.add('bubble-container');
    container.vAlign = "center";
    container.align = 'center';
    // append bubbles
    for (i = 1; i <= 5; i++) {
        let bubble = document.createElement('div');
        bubble.id = i;
        bubble.classList.add('bubble');
        bubble.onclick = function() { bubbleManager(this); };
        container.append(bubble)
    }
    // append input
    let input = document.createElement('input');
    input.classList.add(input_class);
    input.id = attribute_name;
    input.name = attribute_name;
    input.type = "hidden";
    input.value = 0;
    input.form = "build";
    container.appendChild(input);
    return container;
}

//bubbleManager : update bubble object and input associated with it
function bubbleManager(object) {
    let input = object.parentElement.children[5];
    // get parent-object to get input in container
    if (object.classList.contains('bubble-active')) {
        // remove bubbles after clicked bubble
        for (i = 0; i < 5; i++) {
            bubble = object.parentElement.children[i];
            if (bubble.id > object.id) {
                bubble.classList.remove('bubble-active');
            }
        }
        if (object.id == 1) {
            object.classList.remove('bubble-active');
            input.value = 0;
        } else {
            input.value = object.id;
        }
    } else {
        // add bubbles before clicked bubble
        for (i = 4; i >= 0; i--) {
            let bubble = object.parentElement.children[i];
            if (bubble.id <= object.id) {
                bubble.classList.add('bubble-active')
            }
        }
        input.value = object.id;
    }
    // run onchange for element
    if (typeof input.onchange === "function") {
        input.onchange()
    }
}

/* Init */
function init() {
    // iterate through all attribute labels and add bubble-sheet objects
    let i;
    let elements = document.getElementsByClassName('attribute');
    for (i = 0; i < elements.length; i++) {
        let element = elements[i];
        element.parentElement.appendChild(makeBubble(element.htmlFor, 'attribute-input'))
    }
    // iterate through all skill labels and add bubble-sheet objects
    elements = document.getElementsByClassName('skill');
    for (i = 0; i < elements.length; i++) {
        let element = elements[i];
        element.parentElement.parentElement.appendChild(makeBubble(element.htmlFor, 'skill-input'))
    }
    // iterate through all discipline labels to add bubble-sheet objects
    elements = document.getElementsByClassName('discipline');
    for (i = 0; i < elements.length; i++) {
        let element = elements[i];
        element.parentElement.appendChild(makeBubble(element.htmlFor, 'discipline-input'))
    }
}

// run init function
init();
