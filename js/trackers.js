
/* Functions */
//_updateTenBox : update box structures with 10 boxes with divider in center
function _updateTenBox(object, value) {
    let i;
    for (i = 0; i <= 5; i++) {
        let box = object.children[i];
        if (box.id > value) {
            box.classList.remove('bubble-active');
        } else {
            box.classList.add('bubble-active')
        }
    }
    for (i = 6; i <= 10; i++) {
        let box = object.children[i];
        if (box.id > value) {
            box.classList.remove('bubble-active');
        } else {
            box.classList.add('bubble-active')
        }
    }
    // update final object (input) with new value
    object.children[11].value = value;
}

/* Box Trackers */

//updateHealth : update health boxes based on stamina + 3
function updateHealth() {
    // get stamina
    let stamina = document.getElementById('stamina');
    // update boxes according to stamina + 3
    _updateTenBox(
        document.getElementsByClassName('health-container')[0],
        parseInt(stamina.value) + 3,
    );
}

//updateWillpower : update willpower based on resolve + composure
function updateWillpower() {
    // get resolve and composure
    let resolve = document.getElementById('resolve');
    let composure = document.getElementById('composure');
    // update boxes according to resolve + composure
    _updateTenBox(
        document.getElementsByClassName('willpower-container')[0],
        parseInt(resolve.value) + parseInt(composure.value,)
    );
}

/* Discipline Trackers */

// updates powers for the given discipline number in form
function updatePowerCallback(discipline_num, powers) {
    // build select according to power-level to given discipline number
    let powernum = 0;
    let alldivs = [];
    let i, powerdiv, div, select, option;
    for (i=0; i < powers.length; i++) {
        let power = powers[i];
        // update currently selected power-div number if power is next level
        if (power[1] > powernum) {
            powernum += 1;
            powerdiv = document.getElementById('power:'+discipline_num.toString()+','+powernum.toString());
            // spawn select container div and select input
            div = document.createElement('div');
            div.classList.add('custom-select');
            select = document.createElement('select');
            select.id = 'power-select:1,'+powernum.toString();
            select.name = select.id;
            select.form = 'build';
            // append created objects to existing container
            div.appendChild(select);
            powerdiv.appendChild(div);
            alldivs.push(div);
        }
        // add powers to selected container until change occurs
        option = document.createElement('option');
        option.value = power[0];
        option.innerHTML = option.value;
        select.appendChild(option);
    }
    // build custom-select from all created power divs
    for (i=0; i < alldivs.length; i++) {
        buildSelectMenu(alldivs[i]);
    }
}

//updatePowers : do API call and set update callback for given discipline number in form
function updatePowers(discipline_num) {
    let dstr = discipline_num.toString();
    let power_name = document.getElementById('discipline-label-'+dstr).innerHTML;
    let max_power_level = document.getElementById('discipline-'+dstr).value;
    // delete all existing power selects
    let i;
    let powers = document.getElementsByClassName('power-'+dstr);
    for (i=0; i < powers.length; i++) {
        powers[i].innerHTML = '';
    }
    // spawn new powers if there are any
    if (max_power_level > 0 && power_name != '') {
        apiGetPowers(
            power_name,
            max_power_level,
            function (e) { updatePowerCallback(discipline_num, e); },
        );
    }
}

/* Clan Trackers */

//updateDisciplines : update discipline labels based on clan disciplines
// and force update of powers under each discipline now listed
function updateDisciplines(disciplines) {
    // find required discipline sections
    let i;
    for (i=0; i < disciplines.length; i++) {
        // update given discipline values
        let discipline = disciplines[i];
        let label = document.getElementById('discipline-label-'+(i+1).toString());
        console.log(discipline, label);
        label.innerHTML = discipline;
        // update powers under new discipline value
        updatePowers(i+1);
    }
}

/* Init */
function init() {
    // enable box trackers
    let stamina = document.getElementById('stamina');
    let resolve = document.getElementById('resolve');
    let composure = document.getElementById('composure');
    stamina.onchange = function() { updateHealth(); };
    resolve.onchange = function() { updateWillpower(); };
    composure.onchange = function(){ updateWillpower(); };
    // build discipline trackers
    let i;
    for (i=1; i <= 6; i++) {
        let discipline = document.getElementById('discipline-'+i.toString());
        discipline.onchange = function() {
            // parse discipline number from id
            updatePowers(parseInt(this.id.split('-')[1]));
        }
    }
    // build clan trackers
    let clan = document.getElementById('clan');
    clan.onselect = function(e) { apiGetDisciplines(e.innerHTML, updateDisciplines); };
    // run trackers once to get base value
    clan.onselect(clan.firstChild);
    stamina.onchange();
    composure.onchange();
}

// run init
init();